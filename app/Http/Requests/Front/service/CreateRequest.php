<?php

namespace App\Http\Requests\Front\service;

use App\Http\Requests\CoreRequest;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reference_number' =>'required',
            'aadhar_no' =>'required',
            'pan_no' =>'required',
            'first_name' =>'required',
            'last_name' =>'required',
            'email' =>'required|email|unique:digipay_point_service',
            'mobile_no' =>'required|number|max:10',
            'refer_coupon' =>'required|file',
            'bank_copy' =>'required|file',
        ];
    }
}
