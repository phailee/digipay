<?php

namespace App\Http\Requests\Front\Contact;

use App\Http\Requests\CoreRequest;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4',
            'subject' => 'required|min:8',
            'email' => 'required|email',
            'message' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.min' => 'Please enter at least 4 chars',
            'subject.min' => 'Please enter at least 8 chars of subject',
            'message.required' => 'Please write something for us'
        ];
    }
}
