<?php

namespace App\Http\Controllers\Front;

use App\Classes\Reply;
use App\Http\Requests\Front\Contact\CreateRequest;
use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends BaseController
{

    public function store(CreateRequest $request)
    {
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->subject = $request->subject;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->save();

        return Reply::success('Your message is submitted successfully.');
    }
}
