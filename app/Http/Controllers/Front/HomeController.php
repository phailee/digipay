<?php

namespace App\Http\Controllers\Front;

use App\Classes\Reply;
use App\Models\Banner;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->pageTitle = 'Home';
        $this->teams = User::all();
        return view('front.home.home', $this->data);
    }


    public function changeLanguage (Request $request)
    {
        session(['language' => $request->language]);

        return Reply::success('Language successfully changed.');
    }
}
