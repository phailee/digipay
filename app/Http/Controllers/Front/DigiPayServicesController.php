<?php

namespace App\Http\Controllers\Front;

use App\Classes\Reply;
use App\Http\Requests\Front\service\CreateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DigiPayServicesController extends BaseController
{
    public function create()
    {
        $this->pageTitle = 'Digipay Point Services';
        return view('front.forms.digipay-point-services', $this->data);
    }

    public function store(CreateRequest $request)
    {
        return Reply::success('Your service request is submitted successfully.');
    }
}
