<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticPageController extends BaseController
{
    public function about()
    {
        $this->pageTitle = 'About';

        return view('front.about', $this->data);
    }

    public function moneyTransfer()
    {
        $this->pageTitle = 'Money Transfer';

        return view('front.static.money-transfer', $this->data);
    }

    public function contactUs()
    {
        $this->pageTitle = 'Contact Us';

        return view('front.static.contact-us', $this->data);
    }

    public function career()
    {
        $this->pageTitle = 'Career';

        return view('front.static.career', $this->data);
    }

    public function emitra()
    {
        $this->pageTitle = 'E-mitra';

        return view('front.static.e-mitra', $this->data);
    }

    public function cspService()
    {
        $this->pageTitle = 'About CSP';

        return view('front.static.csp-service', $this->data);
    }

    public function aeps()
    {
        $this->pageTitle = 'AEPS';

        return view('front.static.aeps', $this->data);
    }

    public function recharge()
    {
        $this->pageTitle = 'DTH Mobile Recharge';

        return view('front.static.recharge', $this->data);
    }

    public function travel()
    {
        $this->pageTitle = 'Travel Services';

        return view('front.static.travel', $this->data);
    }

    public function insurance()
    {
        $this->pageTitle = 'Insurance Services';

        return view('front.static.insurance', $this->data);
    }

    public function nsdlPan()
    {
        $this->pageTitle = 'PAN Services';

        return view('front.static.nsdl-pan', $this->data);
    }
}
