<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Front', 'middleware' => ['web']], function () {
    Route::get('/home', ['as' => 'front.home', 'uses' => 'HomeController@index']);
    Route::get('/about', ['as' => 'front.about', 'uses' => 'StaticPageController@about']);
    Route::get('/money-transfer', ['as' => 'front.money-transfer', 'uses' => 'StaticPageController@moneyTransfer']);
    //contact us page routes
    Route::get('/contact-us', ['as' => 'front.contact-us', 'uses' => 'StaticPageController@contactUs']);
    Route::post('/contact-us-submit', ['as' => 'front.contact-us-submit', 'uses' => 'ContactController@store']);
    Route::get('/career', ['as' => 'front.career', 'uses' => 'StaticPageController@career']);
    Route::get('/e-mitra', ['as' => 'front.e-mitra', 'uses' => 'StaticPageController@emitra'] );
    Route::get('/csp-service', ['as' => 'front.csp-service', 'uses' => 'StaticPageController@cspService' ]);
    Route::get('/aeps', ['as' => 'front.aeps', 'uses' => 'StaticPageController@aeps' ]);
    Route::get('/recharge', ['as' => 'front.recharge', 'uses' => 'StaticPageController@recharge' ]);
    Route::get('/travel-services', ['as' => 'front.travel-services', 'uses' => 'StaticPageController@travel' ]);
    Route::get('/insurance', ['as' => 'front.insurance', 'uses' => 'StaticPageController@insurance' ]);
    Route::get('/nsdl-pan-branch', ['as' => 'front.nsdl-pan', 'uses' => 'StaticPageController@nsdlPan' ]);
    Route::get('/digipay-point-services', ['as' => 'front.digipay-point-services', 'uses' => 'DigiPayServicesController@create' ]);
    Route::post('/digipay-service-submit', ['as' => 'front.digipay-service-submit', 'uses' => 'DigiPayServicesController@store' ]);
});
