<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDigipayPointServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('digipay_point_service', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference_no');
            $table->string('aadhar_no');
            $table->string('pan_no');
            $table->string('full_name');
            $table->string('email')->unique();
            $table->string('mobile_no');
            $table->string('refer_coupon');
            $table->string('bank_account');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('digipay_point_service');
    }
}
