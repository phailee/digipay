<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::beginTransaction();

        \DB::table('users')->delete();

        \DB::statement('ALTER TABLE users AUTO_INCREMENT = 1');

        \App\Models\User::create([
            'name' => 'Bharat Kumar',
            'email' => 'bhart@digipay.com',
            'designation' => 'DIRECTOR / CEO',
            'about' => 'Bharat is the Founder of DPP Private Limited, India’s fastest growing online B2B company',
            'avatar' => 'bharat.jpg',
            'twitter_link' => 'https://twitter.com/pay_digi',
            'facebook_link' => 'https://www.facebook.com/Digi-Pay-Point-529416087487705/?hc_ref=ARSXjrjaii_1GqybMEzJj-jTeulKwWO35yMFX7JdrMj87jyoOnFFKbK7LceNaQrYxi4&fref=nf',
        ]);

        \App\Models\User::create([
            'name' => 'Manisha Jain',
            'email' => 'manisha@digipay.com',
            'designation' => 'CMD / HR',
            'about' => 'Manisha Jain is Education in M.Com and 7 Year Exp. in Finance',
            'avatar' => 'manisha.jpg',
            'twitter_link' => 'https://twitter.com/pay_digi',
            'facebook_link' => 'https://www.facebook.com/Digi-Pay-Point-529416087487705/?hc_ref=ARSXjrjaii_1GqybMEzJj-jTeulKwWO35yMFX7JdrMj87jyoOnFFKbK7LceNaQrYxi4&fref=nf',
        ]);

        \App\Models\User::create([
            'name' => 'Nitish Kumar',
            'email' => 'nitish@digipay.com',
            'designation' => 'PROJECT MANAGER',
            'about' => 'He has over 3 years of work experience. His Key Areas of Expertise are in Sales & Channel Management, and Retail',
            'avatar' => 'default.jpg',
            'twitter_link' => 'https://twitter.com/pay_digi',
            'facebook_link' => 'https://www.facebook.com/Digi-Pay-Point-529416087487705/?hc_ref=ARSXjrjaii_1GqybMEzJj-jTeulKwWO35yMFX7JdrMj87jyoOnFFKbK7LceNaQrYxi4&fref=nf',
        ]);

        \App\Models\User::create([
            'name' => 'Sanjay Sharma',
            'email' => 'snajay@digipay.com',
            'designation' => 'SMD NATIONAL HEAD',
            'about' => 'Has over 4 years of experience in  Team Management and MIS & OPERATIONS',
            'avatar' => 'sanjay.jpg',
            'twitter_link' => 'https://twitter.com/pay_digi',
            'facebook_link' => 'https://www.facebook.com/Digi-Pay-Point-529416087487705/?hc_ref=ARSXjrjaii_1GqybMEzJj-jTeulKwWO35yMFX7JdrMj87jyoOnFFKbK7LceNaQrYxi4&fref=nf',
        ]);

        \DB::commit();
    }
}
