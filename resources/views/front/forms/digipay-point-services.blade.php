@extends('front.layouts.front-layout')
@section('style')
    <style>
        .panel-default>.panel-heading {
            color: #fff;
            background-color: #000000;
            border-color: #ddd;
            font-size: 20px;
            font-weight: 700;
        }
        .required-label {
            color: #ff0000;
            font-weight: 400;
        }
        .margin-bottom {
            margin-bottom:26px;
        }
    </style>
    <link href="{{ asset('plugins/froiden-helper/helper.css') }}" rel="stylesheet"/>
@endsection

@section('content')
    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ secure_asset('img/static/contact-us.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="contact" class="contact-area">
        <div class="contact-inner area-padding">
            <div class="contact-overly"></div>
            <div class="container ">
                <div class="row">
                    <!-- Start  contact -->
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-plus" aria-hidden="true"></i>  Apply for Digipay Point Service </div>
                            <div class="panel-body">
                                <div class="form contact-form">
                                    <div id="sendmessage">Your message has been sent. Thank you!</div>
                                    <div id="errormessage"></div>
                                    <form action="" method="post" role="form" class="contactForm" id="digipay-service-submit">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <p>Reference Number <span class="required-label">*</span></p>
                                                    <input type="text" name="reference_number" class="form-control" id="reference_number" placeholder="Your Reference Number"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <p>Aadhar No <span class="required-label">*</span></p>
                                                    <input type="number" name="aadhar_no" class="form-control" id="aadhar_no" placeholder="Your Aadhar No" />
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <p>PAN No <span class="required-label">*</span></p>
                                                    <input type="text" name="pan_no" class="form-control" id="pan_no" placeholder="Your Name"/>

                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <p>First Name <span class="required-label">*</span></p>
                                                    <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Your First Name"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <p>Last Name <span class="required-label">*</span></p>
                                                    <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Your Last Name" />
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <p>Email <span class="required-label">*</span></p>
                                                    <input type="email" name="email" class="form-control" id="email" placeholder="Your Email"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <p>Mobile No <span class="required-label">*</span></p>
                                                    <input type="number" name="mobile_no" class="form-control" id="mobile_no" placeholder="Your First Name"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <p>Refer Coupon <span class="required-label">*</span></p>
                                                    <input type="file" name="refer_coupon" id="refer_coupon" placeholder="Your Last Name" />
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <p>Bank A/C <span class="required-label">*</span></p>
                                                    <input type="file" name="bank_copy" id="bank_copy" placeholder="Your Email"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="text-center margin-bottom"><button type="submit" onclick="digipayService();return false;">Apply</button></div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- End Left contact -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts-footer')
    <script src="{{ asset('plugins/froiden-helper/helper.js')}}"></script>
    <script>
        function digipayService(){
            $.easyAjax({
                url: "{!! route('front.digipay-service-submit') !!}",
                type: "POST",
                data: $("#digipay-service-submit").serialize(),
                container: "#digipay-service-submit",
                // messagePosition: "inline",
                success: function (response) {
                    if(response.status == 'success') {
                        $('#digipay-service-submit')[0].reset();
                        // $('#contact-form').remove();
                    }
                }
            });
        }
    </script>
@endsection
