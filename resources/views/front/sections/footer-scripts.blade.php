<script src="{{ secure_asset('lib/jquery/jquery.min.js') }}"></script>
<script src="{{ secure_asset('lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ secure_asset('lib/owlcarousel/owl.carousel.min.js') }}"></script>
<script src="{{ secure_asset('lib/venobox/venobox.min.js') }}"></script>
<script src="{{ secure_asset('lib/knob/jquery.knob.js') }}"></script>
<script src="{{ secure_asset('lib/wow/wow.min.js') }}"></script>
<script src="{{ secure_asset('lib/parallax/parallax.js') }}"></script>
<script src="{{ secure_asset('lib/easing/easing.min.js') }}"></script>
<script src="{{ secure_asset('lib/nivo-slider/js/jquery.nivo.slider.js') }}" type="text/javascript"></script>
<script src="{{ secure_asset('lib/appear/jquery.appear.js') }}"></script>
<script src="{{ secure_asset('lib/isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ secure_asset('//maps.googleapis.com/maps/api/js?key=AIzaSyANW_-7oozZqvpVkyssgYgFJjk6_s4cMVE') }}"></script>

<!-- Contact Form JavaScript File -->
<script src="{{ secure_asset('contactform/contactform.js') }}"></script>

<script src="{{ secure_asset('js/main.js') }}"></script>

<!-- Add csrf protect to each form  -->
<script>
    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }
    });

    <!-- header end -->

    $(function () {
        $('body').on('click', 'a.dropdown-toggle', function (e) {
            $('.collapse').addClass('in');
        })
        $('.dropdown-submenu').on('click', '[data-toggle="dropdown"]', function () {
            console.log($(this));

            $(this).addClass('open');
            setTimeout(function () {
                $('#sub-memu-level2').addClass('open');
            }, 1);


        });
    });
</script>

@yield('footerjs')
@stack('scripts')