<style>
    .dropdown-submenu {

        position:relative;

    }

    .dropdown-submenu > .dropdown-menu {

        top:0;left:100%;

        margin-top:-6px;

        margin-left:-1px;

        border-radius:0 6px 6px 6px;
        -webkit-border-radius:0 6px 6px 6px;

        -moz-border-radius:0 6px 6px 6px;
        width: min-content;

    }

    .dropdown-submenu > a .fa {
        float: right;
    }


    /*.dropdown-submenu > a:after {

        border-color: transparent transparent transparent #fff;

        border-style: solid;

        border-width: 5px 0 5px 5px;

        content: " ";

        display: block;

        float: right;

        height: 0;

        margin-right: -10px;

        margin-top: 5px;

        width: 0;

    }*/



    /*.dropdown-submenu:hover > a:after {

        border-left-color:#555;

    }*/

    @media (max-width: 767px) {

        .dropdown-menu {

            padding-left: 10px;

        }

        .dropdown-menu .dropdown-menu {

            padding-left: 20px;

        }

        .dropdown-menu .dropdown-menu .dropdown-menu {

            padding-left: 30px;

        }

        li.dropdown.open {

            border: 0px solid red;

        }



    }

    @media (min-width: 768px) {

        ul.nav li:hover > ul.dropdown-menu {

            display: block;

        }

        #navbar-example {

            text-align: center;

        }

    }

</style>
<header>
    <!-- header-area start -->
    <div id="sticker" class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <!-- Navigation -->
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- Brand -->
                            <a class="navbar-brand page-scroll sticky-logo" href="{{ route('front.home') }}">
                                <img src="{{ secure_asset('logo/logo.png') }}" class="img-responsive">
                            </a>
                        </div>
                        @include('front.sections.navbar')
                    </nav>
                    <!-- END: Navigation -->
                </div>
            </div>
        </div>
    </div>
    <!-- header-area end -->
</header>
