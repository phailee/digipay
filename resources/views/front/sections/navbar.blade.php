<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
    <ul class="nav navbar-nav navbar-right">
        <li class="@if(strpos(\Request::route()->getName(),'home') !== false ) active @endif">
            <a class="page-scroll" href="{{ route('front.home') }}">Home</a>
        </li>
        <li class="@if(strpos(\Request::route()->getName(),'about') !== false ) active @endif">
            <a class="page-scroll" href="{{ route('front.about') }}">About</a>
        </li>
        <li class="dropdown @if((strpos(\Request::route()->getName(),'money-transfer') !== false) || (strpos(\Request::route()->getName(),'csp-service') !== false) || (strpos(\Request::route()->getName(),'e-mitra') !== false)) active @endif" id="sub-memu-level2">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <b class="caret"></b></a>

            <ul class="dropdown-menu">

                <li class="dropdown dropdown-submenu">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Digipay Point  <i class="fa fa-angle-right" aria-hidden="true"></i></a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('front.money-transfer') }}">
                                Money Transfer
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('front.aeps') }}">
                                AEPS
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('front.recharge') }}">
                                DTH & Mobile Recharge
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('front.nsdl-pan') }}">
                                NSDL Pan Branch
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('front.insurance') }}">
                                Insurance
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('front.travel-services') }}">
                                Travel Service
                            </a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="{{ route('front.csp-service') }}">
                        Banking CSP Service
                    </a>
                </li>
                <li>
                    <a href="{{ route('front.e-mitra') }}">
                        E-Mitra
                    </a>
                </li>
            </ul>
        </li>

        <li class="@if(strpos(\Request::route()->getName(),'career') !== false ) active @endif">
            <a class="page-scroll" href="{{ route('front.career') }}">Career</a>
        </li>

        <li>
            <a class="page-scroll" href="http://digipaypoint.in/">Login</a>
        </li>

        <li>
            <a class="page-scroll" href="{{ route('front.about') }}">Download</a>
        </li>

        <li class="@if(strpos(\Request::route()->getName(),'contact-us') !== false ) active @endif">
            <a class="page-scroll" href="{{ route('front.contact-us') }}">Contact</a>
        </li>
        <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Apply Online <b class="caret"></b>
            </a>

            <ul class="dropdown-menu">
                <li class="dropdown dropdown-submenu">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Banking Kiosk CSP <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="#">
                                SBI Baking Kiosk
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                PNB Banking Kiosk
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Bank of Baroda Kiosk
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                BOI Kiosk
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                RMGB Kiosk
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                E-Mitra Kiosk
                            </a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="{{ route('front.digipay-point-services') }}">
                        DigiPay Point Service
                    </a>
                </li>
                <li>
                    <a href="#">
                        E-Mitra Service
                    </a>
                </li>
            </ul>
        </li>
        <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Upcomming Services <b class="caret"></b>
            </a>

            <ul class="dropdown-menu">

                <li>
                    <a href="#">
                        GST Registration
                    </a>
                </li>
                <li>
                    <a href="#">
                        GST Return False
                    </a>
                </li>
                <li>
                    <a href="#">
                        Travel Booking
                    </a>
                </li>
                <li>
                    <a href="#">
                        Passport Apply
                    </a>
                </li>
                <li>
                    <a href="#">
                        Voter Card Apply
                    </a>
                </li>

            </ul>
        </li>
    </ul>
</div>
<!-- navbar-collapse -->