{{--
<link href="img/favicon.png" rel="icon">
<link href="img/apple-touch-icon.png" rel="apple-touch-icon">
--}}

<!-- Google Fonts -->
<link href="{{ secure_asset('https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet') }}">

<!-- Bootstrap CSS File -->
<link href="{{ secure_asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

<!-- Libraries CSS Files -->

<link href="{{ secure_asset('lib/nivo-slider/css/nivo-slider.css') }}" rel="stylesheet">
<link href="{{ secure_asset('lib/owlcarousel/owl.carousel.css') }}" rel="stylesheet">
<link href="{{ secure_asset('lib/owlcarousel/owl.transitions.css') }}" rel="stylesheet">
<link href="{{ secure_asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ secure_asset('lib/animate/animate.min.css') }}" rel="stylesheet">
<link href="{{ secure_asset('lib/venobox/venobox.css') }}" rel="stylesheet">

<!-- Nivo Slider Theme -->
<link href="{{ secure_asset('css/nivo-slider-theme.css') }}" rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="{{ secure_asset('css/style.css') }}" rel="stylesheet">

<!-- Responsive Stylesheet File -->
<link href="{{ secure_asset('css/responsive.css') }}" rel="stylesheet">

<!-- =======================================================
  Theme Name: eBusiness
  Theme URL: https://bootstrapmade.com/ebusiness-bootstrap-corporate-template/
  Author: BootstrapMade.com
  License: https://bootstrapmade.com/license/
======================================================= -->
<style>
    .has-error {
        border: unset !important;
        color: #de2f2c !important;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075)!important;
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
    }

    .header-area {
        background: #fff;
    }
    .navbar-header a.navbar-brand {
        padding: 1px 0;
        margin: 0px 0px 0px -50px;
    }
    @media (min-device-width:768px) and (max-device-width:1024px){
        .navbar-header a.navbar-brand {
            padding: 10px 0;
            margin: 0px 0px 0px 0px;
        }

        .wellcome-text {
            margin: 25px 0 -50px 0;
            padding: 70px 0px;
        }
    }

    #sticker .main-menu ul.navbar-nav li.dropdown ul li a {
        color: #555;
        padding: 5px 2px;
    }
    #sticker .main-menu ul.navbar-nav li.dropdown ul li {
        border-bottom: solid 1px #ddd;
        width: 100%
    }
    #navbar-example.in ul.navbar-nav li.dropdown ul li {
        border-bottom: none !important;
        width: 100%
    }

    #navbar-example.in ul.navbar-nav li.dropdown ul li a {
        color: #fff;
        padding: 5px 2px;
    }

    #sticker .main-menu ul.navbar-nav li.dropdown ul li:last-child {
        border-bottom: none;
    }

    #sticker {
        height: 70px;
    }
    @media (max-width: 1200px) and (min-width: 767px) {
        #navbar-example {
            width: calc(100% - 250px) !important;
        }
        #sticker {
            height: 140px !important;
        }

        #navbar-example {
            position: absolute;
            right: 0;
            top: 0;
        }
        .wellcome-text {
            margin: 100px 0 -50px 0;
            padding: 70px 0px;
        }
    }
    .main-menu ul.navbar-nav li.active a {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
         color: #555;
        position: relative;
    }
    .main-menu ul.navbar-nav li a {
        color: #555;
    }
    .main-menu.collapse.in ul.navbar-nav li a {
        color: #fff;
    }
    .main-menu ul.navbar-nav li.active a:hover {
        background: none;
        color: #3EC1D5;
    }

    .navbar-default .navbar-toggle .icon-bar {
        background-color: #888;
        width: 30px;
        height: 2px;
    }

    @media (max-width: 767px) {
        .navbar-header a.navbar-brand, .stick .navbar-header a.navbar-brand {
            padding: 0px 2px !important;
            margin: 0px;
        }
        .collapse .navbar-collapse .main-menu .bs-example-navbar-collapse-1 {
            max-width: 500px;
        }

        .wellcome-text {
            margin: 25px 0 -50px 0;
            padding: 70px 0px;
        }

    }
    .header-area.stick {
        height: auto;
    }

    .faq-details h4.check-title a {
        font-size: 15px;
        font-weight: 600;
    }

</style>
@yield('style')
