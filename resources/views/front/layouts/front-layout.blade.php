<!DOCTYPE html>
<html>
<head>
   @include('front.sections.meta-data')
   @include('front.sections.style')
</head>
<body>
<div id="preloader"></div>
@include('front.sections.header')
@include('front.sections.navbar')

<!-- Main content -->

@yield('content')

    <!-- /.content -->

<!-- Models -->
@include('front.sections.modals')
<!-- /.Models -->

@yield('modals')

<!-- /.content-wrapper -->

     {{--Footer section--}}
@include('front.sections.footer')

<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
@include('front.sections.footer-scripts')
@yield('scripts-footer')


<!-- E o tooltip menu -->
</body>
</html>
