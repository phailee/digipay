<!-- Start team Area -->
<div id="team" class="our-team-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <h2>Our special Team</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="team-top">
                @foreach($teams as  $team)
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="single-team-member">
                            <div class="team-img">
                                <a href="javascript:;">
                                    <img src="{{ secure_asset('img/team/'.$team->avatar) }}" alt="">
                                </a>
                                <div class="team-social-icon text-center">
                                    <h6>{{ $team->about }}</h6>
                                    <ul>
                                        <li>
                                            <a target="_blank" href="{{ $team->facebook_link }}">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="{{ $team->twitter_link }}">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>{{ $team->name }}</h4>
                                <p>{{ $team->designation }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
<!-- End Team Area -->