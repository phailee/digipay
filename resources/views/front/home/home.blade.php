@extends('front.layouts.front-layout')
@section('style')
<style>
    .testi-text h5 {
        color: #fff;
        font-size: 15px;
    }
    .team-social-icon {
        left: 0%;
        margin-left: 10px;
        margin-right: 10px;
        margin-top: -50px;
    }
    .team-social-icon > h6 {
        color:#fff;
    }
</style>
@endsection

@section('content')
    @include('front.home.slider')
    @include('front.home.services')
    @include('front.home.team')
    @include('front.home.testimonials')
    @include('front.home.client')
@endsection

@section('scripts-footer')
    <!-- Sticky menu -->
<script>

</script>
@endsection
