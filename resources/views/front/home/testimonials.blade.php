<!-- Start Testimonials -->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="section-headline text-center">
            <h2>Customers reviews</h2>
        </div>
    </div>
</div>
<div class="testimonials-area">
    <div class="testi-inner area-padding">
        <div class="testi-overly"></div>
        <div class="container ">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <!-- Start testimonials Start -->
                    <div class="testimonial-content text-center">
                        <a class="quate" href="#"><i class="fa fa-quote-right"></i></a>
                        <!-- start testimonial carousel -->
                        <div class="testimonial-carousel">
                            <div class="single-testi">
                                <div class="testi-text">
                                    <p>
                                        Digipay Point has helped me a lot in terms of revenue generation and right now i can earn sufficient for my family.
                                    </p>
                                    <h6>Jaydeep Sing</h6>
                                    <h5>Distributer , Jodhpur</h5>
                                </div>
                            </div>
                            <!-- End single item -->
                            <div class="single-testi">
                                <div class="testi-text">
                                    <p>
                                        The Business Model is good, and the teams are very hard working.
                                    </p>
                                    <h6>Rajshree Sharma</h6>
                                    <h5>Retailer, Kolkata</h5>
                                </div>
                            </div>
                            <!-- End single item -->
                            <div class="single-testi">
                                <div class="testi-text">
                                    <p>
                                        Best team support in Digipaypoint. any issues is no pending within 24 hours anyhow.

                                    </p>
                                    <h6>Sunil Kumar</h6>
                                    <h5>Distributer, Bihar</h5>
                                </div>
                            </div>
                            <!-- End single item -->
                            <div class="single-testi">
                                <div class="testi-text">
                                    <p>
                                        The services of Digi Paypoint is robust. The steps are simple and user friendly.

                                    </p>
                                    <h6>
                                        Varun Kumar</h6>
                                    <h5>Retailer, Delhi</h5>
                                </div>
                            </div>
                            <!-- End single item -->
                        </div>
                    </div>
                    <!-- End testimonials end -->
                </div>
                <!-- End Right Feature -->
            </div>
        </div>
    </div>
</div>
<!-- End Testimonials -->