<!-- Start Service area -->
<div id="services" class="services-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline services-head text-center">
                    <h2>Our Services</h2>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="services-contents">
                <!-- Start Left services -->
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <!-- end col-md-4 -->
                        <div class=" about-move">
                            <div class="services-details">
                                <div class="single-services">
                                    <a class="services-icon" href="#">
                                        <i class="fa fa-camera-retro"></i>
                                    </a>
                                    <h4>Aadhar Enabled Payment System (AEPS) </h4>
                                    <p>
                                        DigiPay Point is providing AEPS service platform, where we allows you to send money instantly through your adhaar to any Adhaar enabled bank’s account across India.
                                    </p>
                                </div>
                            </div>
                            <!-- end about-details -->
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="about-move">
                            <div class="services-details">
                                <div class="single-services">
                                    <a class="services-icon" href="#">
                                        <i class="fa fa-camera-retro"></i>
                                    </a>
                                    <h4>Money Transfer</h4>
                                    <p>
                                        DigiPay Point is providing Domestic Money Transfer service platform, where we allows you to send money instantly to any IMPS & NEFT supported banks across India..
                                    </p>
                                </div>
                            </div>
                            <!-- end about-details -->
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <!-- end col-md-4 -->
                        <div class=" about-move">
                            <div class="services-details">
                                <div class="single-services">
                                    <a class="services-icon" href="#">
                                        <i class="fa fa-wordpress"></i>
                                    </a>
                                    <h4>Recharge</h4>
                                    <p>
                                        Easy & Fastest way to Recharge for Datacard with all operators. Online Recharge made easy with Mystic Money any time.
                                    </p>
                                </div>
                            </div>
                            <!-- end about-details -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <!-- end col-md-4 -->
                        <div class=" about-move">
                            <div class="services-details">
                                <div class="single-services">
                                    <a class="services-icon" href="#">
                                        <i class="fa fa-bar-chart"></i>
                                    </a>
                                    <h4>Pan Card</h4>
                                    <p>
                                        DigiPay Point is providing Pancard service platform, where we allows you to provide pancards to your customers through UTI and even through NSDL also across India.
                                    </p>
                                </div>
                            </div>
                            <!-- end about-details -->
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="about-move">
                            <div class="services-details">
                                <div class="single-services">
                                    <a class="services-icon" href="#">
                                        <i class="fa fa-code"></i>
                                    </a>
                                    <h4>SBI CSP</h4>
                                    <p>
                                        A large number of people, living on the boundary line of the socioeconomic structure, particularly the lower income group (LIG), economically weaker society (EWS), laborers, agriculture/ factory workers and women do not have a savings account and moreover they are not able to open such accounts due to lack of valid address and ID proof. As a result they face difficulties in parking their hard earned money in a safer place.
                                    </p>
                                </div>
                            </div>
                            <!-- end about-details -->
                        </div>
                    </div>
                    <!-- End Left services -->
                </div>


            </div>
        </div>
    </div>
</div>
<!-- End Service area -->