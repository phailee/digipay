<!-- Start Slider Area -->
<div id="home" class="slider-area">
    <div class="bend niceties preview-2">
        <div id="ensign-nivoslider" class="slides">
            <img src="{{ secure_asset('img/slider/slider1.jpg') }}" alt="" title="#slider-direction-1" />
            <img src="{{ secure_asset('img/slider/slider2.jpg') }}" alt="" title="#slider-direction-2" />
            <img src="{{ secure_asset('img/slider/slider3.jpg') }}" alt="" title="#slider-direction-3" />
            <img src="{{ secure_asset('img/slider/slider4.jpg') }}" alt="" title="#slider-direction-4" />
{{--            <img src="{{ asset('img/slider/slider5.png') }}" alt="" title="#slider-direction-3" />--}}
        </div>

        <!-- direction 1 -->
        <div id="slider-direction-1" class="slider-direction slider-one">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-content">
                            <!-- layer 1 -->
                            <div class="layer-1-1 hidden-xs wow slideInDown" data-wow-duration="2s" data-wow-delay=".2s">
                                <h2 class="title1">
                                    Become Bank Mitra (CSP)
                                </h2>
                            </div>
                            <!-- layer 2 -->
                            <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                                <h1 class="title2">
                                    An Indian citizen having basic computer knowledge and above age of 18 years can become a Bank Mitra for Bank and start their own entrepreneurial journey.</h1>
                            </div>
                            <!-- layer 3 -->
                            <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                                <a class="ready-btn right-btn page-scroll" href="#services">
                                    Read More
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- direction 2 -->
        <div id="slider-direction-2" class="slider-direction slider-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-content text-center">
                            <!-- layer 1 -->
                            <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                                <h2 class="title1">
                                    NSDL Pan Service
                                </h2>
                            </div>
                            <!-- layer 2 -->
                            <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                                <h1 class="title2">
                                    Provide in all india
                                <b>Nsdl Pan Branch </b> Franchise. Pan No. Ganrate Apply within 48 Hour’s
                                </h1>
                            </div>
                            <!-- layer 3 -->
                            <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                                <a class="ready-btn right-btn page-scroll" href="#services">
                                    Read More
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- direction 3 -->
        <div id="slider-direction-3" class="slider-direction slider-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-content">
                            <!-- layer 1 -->
                            <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                                <h2 class="title1">

                                    Aadhaar Enabled Payment System [AEPS]

                                </h2>
                            </div>
                            <!-- layer 2 -->
                            <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                                <h1 class="title2">
                                    Congratulations </br>
                                    AEPS Service now Start with NPCI & RBL Bank.
                                </h1>
                                {{--<ol>--}}
                                    {{--<li>Cash Withdrawal</li>--}}
                                    {{--<li>Cash Deposit</li>--}}
                                    {{--<li>Balance Enquiry</li>--}}
                                {{--</ol>--}}
                            </div>
                            <!-- layer 3 -->
                            <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                                <a class="ready-btn right-btn page-scroll" href="#services">
                                    Read More
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- direction 4 -->
        <div id="slider-direction-4" class="slider-direction slider-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-content">
                            <!-- layer 1 -->
                            <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                                <h2 class="title1">
                                    Money Transfer
                                </h2>
                            </div>{{--<ol>--}}
                                    {{--<li>Cash Withdrawal</li>--}}
                                    {{--<li>Cash Deposit</li>--}}
                                    {{--<li>Balance Enquiry</li>--}}
                                {{--</ol>--}}
                            <!-- layer 2 -->
                            <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                                <h1 class="title2">Congratulations </br>
                                    DPP Provide Money Transfer Service in India Low Cost.
                                </h1>
                            </div>
                            <!-- layer 3 -->
                            <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                                <a class="ready-btn right-btn page-scroll" href="#services">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Slider Area -->