<!-- Start team Area -->
<div id="team" class="our-team-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <h2>Our Clients</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="team-top">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="single-team-member">
                            <div class="team-img">
                                <a href="javascript:;">
                                    <img src="{{ secure_asset('img/clients/nsdl.jpg') }}" alt="">
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="single-team-member">
                            <div class="team-img">
                                <a href="javascript:;">
                                    <img src="{{ secure_asset('img/clients/aeps.png') }}" alt="">
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="single-team-member">
                            <div class="team-img">
                                <a href="javascript:;">
                                    <img src="{{ secure_asset('img/clients/money.png') }}" alt="">
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="single-team-member">
                            <div class="team-img">
                                <a href="javascript:;">
                                    <img src="{{ secure_asset('img/clients/csp.png') }}" alt="">
                                </a>

                            </div>
                        </div>
                    </div>


            </div>
        </div>
    </div>
</div>
<!-- End Team Area -->