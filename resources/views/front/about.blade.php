@extends('front.layouts.front-layout')
@section('style')
    <style>
        .wellcome-area {
            background: rgba(248, 248, 248, 0.8) url("{{ secure_asset('img/static/about.jpg') }}");
            background-size: cover;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .section-headline p:last-child::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
            margin-bottom: 50px;
        }
        .section-headline p:last-child {
            padding-bottom: 40px;
        }
        .section-headline h2::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0px;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
        }
    </style>
@endsection

@section('content')
    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ secure_asset('img/static/about.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- start pricing area -->
    <div id="pricing" class="pricing-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Digipay Point Business Correspondent Services</h2>
                        <p><strong>Digipay Point</strong>&nbsp;enabling you to act as a banker in your neighborhood. By being part of&nbsp;<strong>digipaypoint’s</strong>network you will be able to provide your customers all the services of a bank branch and also offer additional services such as travel bookings and bill payments among others.</p>
                        <p><strong>Digipay Point</strong>&nbsp;platform allows access through mobile and desktop devices resulting in low or no investment. We provide attractive commission on every transaction helping you in having an additional source of income and an increase in footfalls at your establishment.&nbsp;<strong>Digipay Point&nbsp;</strong>allows you the opportunity to have higher income without heavy investments along with earning the respect of being the neighborhood banker.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="pri_table_list active">
                        <h3>Key Benefits </h3>
                        <ol>
                            <li class="check">Attractive commission on every transaction</li>
                            <li class="check"><strong>Single account settlement</strong>: Advantage of all banking transactions and settlements in a single account</li>
                            <li class="check"><strong>Elevated status </strong>: Earn the respect of being a banker</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="pri_table_list active">
                        {{--<span class="saleon">top sale</span>--}}
                        <h3>What all you can do? </h3>
                        <ol>
                            <li class="check">Domestic Money Transfer</li>
                            <li class="check">Nepal Money Transfer</li>
                            <li class="check">Aadhaar Enabled Payment</li>
                            <li class="check">System (AEPS)</li>
                            <li class="check">Cash at Point of Sale (POS)</li>
                            <li class="check">Bill Payments-BBPS</li>
                            <li class="check">Mobile / DTH Recharge</li>
                            <li class="check">Cash Management Services </li>
                            <li class="check">Cash Management Services (Multiple Clients)</li>
                            <li class="check">Travel Bookings (Air / Rail / Hotel)</li>
                            <li class="check">Insurance</li>
                        </ol>
                        {{--<button>sign up now</button>--}}
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="pri_table_list active">
                        <h3>Eligibilty</h3>
                        <ol>
                            <li> <strong>Age</strong>: 18 years and above</li>
                            <li><strong>Area of operation</strong>: Vicinity of the shop</li>
                        </ol>
                        {{--<button>sign up now</button>--}}
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="pri_table_list active">
                        <h3>Key Benefits </h3>
                        <ol>
                            <li class="check">Attractive commission on every transaction</li>
                            <li class="check"><strong>Single account settlement</strong>: Advantage of all banking transactions and settlements in a single account</li>
                            <li class="check"><strong>Elevated status </strong>: Earn the respect of being a banker</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End table area -->
@endsection

@section('scripts-footer')
    <!-- Sticky menu -->
    <script>

    </script>
@endsection
