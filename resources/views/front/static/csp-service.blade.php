@extends('front.layouts.front-layout')
@section('style')
    <style>
        .wellcome-area {
            background: rgba(248, 248, 248, 0.8) url("{{ secure_asset('img/static/csp.png') }}");
            background-size: cover;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .section-headline p:last-child::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
            margin-bottom: 50px;
        }
        .section-headline p:last-child {
            padding-bottom: 40px;
        }
        .section-headline h2::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0px;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
        }
        .faq-details {
            padding: 30px 0 10px 0;
        }
    </style>
@endsection

@section('content')

    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ secure_asset('img/static/csp.png') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- region service area -->
    <div class="faq-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Banking CSP Service</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div>
                        <div class="" id="p-view-1">
                            <div class="tab-inner">
                                <div class="event-content head-team single-well">
                                    <h4>About CSP</h4>
                                    <p>
                                        A large number of people, living on the boundary line of the socioeconomic structure, particularly the lower income group (LIG), economically weaker society (EWS), laborers, agriculture/ factory workers and women do not have a savings account and moreover they are not able to open such accounts due to lack of valid address and ID proof. As a result they face difficulties in parking their hard earned money in a safer place.
                                    </p>
                                    <p>
                                        <strong>
                                            Through the Kiosk Banking Solution the following services are being offered:
                                        </strong>
                                    </p>

                                    <ul>
                                        <li>
                                            <i class="fa fa-check"></i> Opening of ‘No-Frill Savings Accounts’ by kiosk banking module
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Deposits / Withdrawals of Cash in Savings Accounts
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Opening of Term-Deposit/Recurring Deposit/ SHG Accounts
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Money Transfer to any Bank account
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Aadhaar enabled deposits / withdrawals
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i>  DBT enabled facility in accounts
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i>  Pradhan Mantri Jeevan Jyoti Bima Yojna / Suraksha Bima Yojna
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i>  Atal Pension Yojna
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i>   Loan lead generation & repayments
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="faq-details">
                        <div class="panel-group" id="accordion">
                            <!-- Panel Default -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="check-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#check1">
                                            <span class="acc-icons"></span>ELIGIBILITY FOR OPENING & MAINTAINING OF CSP
                                        </a>
                                    </h4>
                                </div>
                                <div id="check1" class="panel-collapse collapse">
                                    <div class="panel-body single-well">
                                        <ul>
                                            <li>
                                                <i class="fa fa-check"></i> उम्र कम-से-कम 21 वर्ष (AT LEAST 21 YEARS OF AGE)
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> मैट्रिक या उससे ज्यादा पढे-लिखे (MATRIC OR ABOVE)
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> कम्प्यूटर का सामान्य ज्ञान रखने वाले (COMPUTER LITERATE)
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> कार्य मेंअपनी कुछ पूंजी लगा सकने की क्षमता वाले (WILLING TO INVEST SOME MONEY IN PROJECT)
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> जिम्मेवार (RESPONSIBLE)
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  कर्मठ (LABOURIOUS)
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  बेरोजगार ब्यक्ति (UNEMPLOYED)
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Atal Pension Yojna
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>   Loan lead generation & repayments
                                            </li>
                                        </ul>

                                        <div class="single-blog text-center" style="padding: 10px">
                                            <span>
                                                <a href="javascript:;" class="ready-btn">आवेदन करें (APPLY)</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Panel Default -->
                            <!-- Panel Default -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="check-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#check2">
                                            <span class="acc-icons"></span> Banking +
                                        </a>
                                    </h4>
                                </div>
                                <div id="check2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Convert your shop/establishment in a Customer Service Point (CSP) and provide banking, insurance, pension and other related financial services to your customers.
                                        </p>
                                        <p>
                                            <strong>
                                                The Kiosk Banking Business Correspondent (BC)
                                            </strong>
                                            model aims to provide real time, user friendly financial services to the consumers in their neighborhood itself. "Digipay Point" is dedicated to propagate the Kiosk banking services to Metro / Urban / Semi-Urban / Rural / Hilly / Difficult areas and to create new Kiosk Banking agents across the country.
                                        </p>
                                        <div class="single-blog text-center" style="padding: 10px">
                                            <span>
                                                <a href="javascript:;" class="ready-btn">आवेदन करें (APPLY)</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Panel Default -->
                            <!-- Panel Default -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="check-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#check3">
                                            <span class="acc-icons"></span>What does the retailer (CSP / BC-AGENT) get ?
                                        </a>
                                    </h4>
                                </div>
                                <div id="check3" class="panel-collapse collapse">
                                    <div class="panel-body single-well">
                                        <ul>
                                            <li>
                                                <i class="fa fa-check"></i> Association with Banks like SBI / PNB / MBGB
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Bank’s software / portal, User ID (KO ID) and Password
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Biometric Reader & software for electronic thumb impression
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Authorization Certificate of CSP by the BC
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Bank’s Banners, signage, Logo/Stickers/Leaflets/Contact Matrix
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Proper Training of CSP to operate the system by the BC
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Dedicated technical support from the local Supervisors of "Sanjivani"
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Dedicated support from the Help Lines and web portal of "Sanjivani"
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>   Web Software to operate different services (Recharge/Billing/Travel Solution)
                                            </li>
                                        </ul>

                                        <div class="single-blog text-center" style="padding: 10px">
                                            <span>
                                                <a href="javascript:;" class="ready-btn">आवेदन करें (APPLY)</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Panel Default -->
                            <!-- Panel Default -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="check-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#check4">
                                            <span class="acc-icons"></span>Benefits to RETAILER (CSP/BC-A) through KIOSK BANKING:
                                        </a>
                                    </h4>
                                </div>
                                <div id="check4" class="panel-collapse collapse">
                                    <div class="panel-body single-well">
                                        <ul>
                                            <li>
                                                <i class="fa fa-check"></i>  Opportunity to associate with SBI / PNB / MBGB
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Very easy to use application
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Hassle free and simple system
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Real time based transactions
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Very encouraging fee structure for generation of new revenue at CSP level
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Banking, Insurance, Pension and other financial products available for customers
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Opportunity to increase your earnings, goodwill & brand name in the market
                                            </li>
                                        </ul>

                                        <div class="single-blog text-center" style="padding: 10px">
                                            <span>
                                                <a href="javascript:;" class="ready-btn">आवेदन करें (APPLY)</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Panel Default -->

                            <!-- Panel Default -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="check-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#check5">
                                            <span class="acc-icons"></span>What does the retailer (CSP / BC-A) need?
                                        </a>
                                    </h4>
                                </div>
                                <div id="check5" class="panel-collapse collapse">
                                    <div class="panel-body single-well">
                                        <ul>
                                            <li>
                                                <i class="fa fa-check"></i>  Office or Retail Outlet
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Furniture including chairs for customers
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Electricity
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Laptop
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Internet Connectivity
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Printers: (a) General and (b) Thermal
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Web cam
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Finger Print Scanner
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Some Cash for availing Cash-Holding-Limit for successful transactions (at least Rs.25,000/-)
                                            </li>
                                        </ul>

                                        <div class="single-blog text-center" style="padding: 10px">
                                            <span>
                                                <a href="javascript:;" class="ready-btn">आवेदन करें (APPLY)</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Panel Default -->

                            <!-- Panel Default -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="check-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#check6">
                                            <span class="acc-icons">What documents are required FOR CSP CODE?	</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="check6" class="panel-collapse collapse">
                                    <div class="panel-body single-well">
                                        <ul>
                                            <li>
                                                <i class="fa fa-check"></i>  Application form duly signed
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  10 Passport size colorful recent photos
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Educational Certificates Matric onwards
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  PAN Card (Mandatory)
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> AADHAAR Card (Mandatory)
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Voter I Card
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Driving License
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Shop Agreement
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Character / Police Verification Certificate
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Name, Address, Mobile No. and signature of two references
                                            </li>
                                        </ul>

                                        <div class="single-blog text-center" style="padding: 10px">
                                            <span>
                                                <a href="javascript:;" class="ready-btn">आवेदन करें (APPLY)</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Panel Default -->

                            <!-- Panel Default -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="check-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#check7">
                                            <span class="acc-icons">CSP Application - Activation Process</span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="check7" class="panel-collapse collapse">
                                    <div class="panel-body single-well">
                                        <ul>
                                            <li>
                                                <i class="fa fa-check"></i>  Application Form, related documents and Registration fees received by "Sanjivani" team
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Due Diligence Report (DDR) by the "Sanjivani" team
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>   DDR to be filed to Bank’s local branch (Link / Base Branch
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Recommendation of Branch Manager to Regional / Circle Office
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Recommendation of Regional Office to LHO only in case of SBI
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>  Generation / creation of KO Code by the Bank
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Configuration of CSP Code by the "Sanjivani" office
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Capturing of fingers of CSP Operator through Finger Print Scanner by the BC
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> CTerminal mapping & Activation of KO Code by the Bank
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Technical & theoretical Training of CSP by the "Sanjivani" team
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> Link Branch / Base Branch of the Bank is informed by the "Sanjivani" team about activation of CSP
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i> CSP starts functioning
                                            </li>
                                            <li>
                                                <i class="fa fa-check"></i>Entire process usually takes 25-30 days
                                            </li>
                                        </ul>

                                        <div class="single-blog text-center" style="padding: 10px">
                                            <span>
                                                <a href="javascript:;" class="ready-btn">आवेदन करें (APPLY)</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Panel Default -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end Row -->
        </div>
    </div>
    <!-- endregion service area -->
@endsection

@section('scripts-footer')
    <!-- Sticky menu -->
    <script>

    </script>
@endsection
