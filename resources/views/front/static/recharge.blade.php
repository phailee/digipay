@extends('front.layouts.front-layout')
@section('style')
    <style>
        .wellcome-area {
            background: rgba(248, 248, 248, 0.8) url("{{ secure_asset('img/static/recharge.jpg') }}");
            background-size: cover;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .section-headline p:last-child::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
            margin-bottom: 50px;
        }
        .section-headline p:last-child {
            padding-bottom: 40px;
        }
        .section-headline h2::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0px;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
        }
    </style>
@endsection

@section('content')

    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ secure_asset('img/static/recharge.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- start pricing area -->
    <div id="pricing" class="pricing-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Mobile Recharge</h2>
                        <p>Recharge your DTH connection, mobile, data card by walking into any Digipay Point CSP . Get your recharge credited into your account in moments after you pay for it at our branches or select merchant establishments..</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="pri_table_list active">
                        <h3>Key Features </h3>
                        <ol>
                            <li class="check">
                                Recharge any prepaid mobile, data card and DTH</li>
                            <li class="check">Easy, hassle-free and safe</li>
                            <li class="check">Choose plan and get instant recharge</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="pri_table_list active">

                        <h3>Key Benefits</h3>
                        <ol>
                            <li class="check">View all the recharge plans available in a single portal</li>
                            <li class="check">Instant recharge and confirmation from operator</li>
                            <li class="check">No charges applicable on recharge transactions</li>
                        </ol>

                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="pri_table_list active">

                        <h3>Service Provider Category</h3>
                        <ol>
                            <li class="check"><strong>Mobile Recharges </strong>(Aircel, Airtel, BSNL, Idea, MTNL Delhi, MTNL Mumbai, MTS, Reliance, Docomo, Videocon, Vodafone)</li>
                            <li class="check"><strong>DTH Recharges </strong>(Airtel, BIG TV, Dish TV, TATA Sky, SunTV, Videocon)</li>
                            <li class="check"><strong>Data Card </strong>(Aircel, Airtel, BSNL, Idea, MTNL Delhi, MTNL Mumbai, MTS, Reliance, Docomo, Videocon, Vodafone)</li>
                        </ol>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End table area -->
@endsection

@section('scripts-footer')
    <!-- Sticky menu -->
    <script>

    </script>
@endsection
