@extends('front.layouts.front-layout')
@section('style')
    <style>
        .wellcome-area {
            background: rgba(248, 248, 248, 0.8) url("{{ secure_asset('img/static/2.jpg') }}");
            background-size: cover;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .section-headline p:last-child::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
            margin-bottom: 50px;
        }
        .section-headline p:last-child {
            padding-bottom: 40px;
        }
        .section-headline h2::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0px;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
        }
    </style>
@endsection

@section('content')

    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ secure_asset('img/static/2.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- start pricing area -->
    <div id="pricing" class="pricing-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Money Transfer</h2>
                        <p>Fund transfers will now be made easy with Cash to Account. Carry in cash and walk-in to any Digipay Point CSP or select merchant establishments, fill in few details and have the money transferred to your loved ones, anywhere in the country.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="pri_table_list active">
                        <h3>Key Features </h3>
                        <ol>
                            <li class="check">
                                Send money instantly</li>
                            <li class="check">Available 24x7x365</li>
                            <li class="check">Fund transfer can be done on Sundays and bank holidays</li>
                            <li class="check">Instant confirmation to sender via SMS</li>
                            <li class="check">Safe and secure transaction</li>
                            <li class="check">This service is available across all the bank branches</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="pri_table_list active">
                        {{--<span class="saleon">top sale</span>--}}
                        <h3>Key Benefits</h3>
                        <ol>
                            <li class="check">Cost effective</li>
                            <li class="check">Safe and secure transaction</li>
                            <li class="check">Fund transfer to all the PSUs and Private Banks</li>
                            <li class="check">Free SMS alerts on every transaction</li>
                            <li class="check">Near doorstep remittance facility</li>
                        </ol>
                        {{--<button>sign up now</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End table area -->
@endsection

@section('scripts-footer')
    <!-- Sticky menu -->
    <script>

    </script>
@endsection
