@extends('front.layouts.front-layout')
@section('style')
    <style>
        .wellcome-area {
            background: rgba(248, 248, 248, 0.8) url("{{ secure_asset('img/static/travel.jpg') }}");
            background-size: cover;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .line-out p:last-child::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
            margin-bottom: 50px;
        }
        .line-out p:last-child {
            padding-bottom: 40px;
        }
        .section-headline h2::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0px;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
        }
        .faq-details {
            padding: 30px 0 10px 0;
        }
    </style>
@endsection

@section('content')

    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ secure_asset('img/static/travel.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- region service area -->
    <div class="faq-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 line-out">
                    <div class="section-headline text-center">
                        <h2>Travel Services</h2>

                    </div>
                    <p>
                        India’s leading B2B travel portal Digipay Point is India’s largest B2B travel portal, enabling our partners to serve their customers efficiently, with the right pricing and inventory.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div>
                        <div class="" id="p-view-1">
                            <div class="tab-inner">
                                <div class="event-content head-team single-well">
                                    <h4>Key Features</h4>

                                    <ul>
                                        <li>
                                            <i class="fa fa-check"></i> Simple to use, Time saving and cost effective platform
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Competitive Rates and 24 x 7 Personalized Services
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Your company logo and address on the E ticket. Book,Sell and print tickets 24 x 7
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Earn Significant Commissions
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Fare Mark-up tool to enable you to earn more Commissions
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end Row -->
        </div>
    </div>
    <!-- endregion service area -->
@endsection

@section('scripts-footer')
    <!-- Sticky menu -->
    <script>

    </script>
@endsection
