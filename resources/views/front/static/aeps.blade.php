@extends('front.layouts.front-layout')
@section('style')
    <style>
        .wellcome-area {
            background: rgba(248, 248, 248, 0.8) url("{{ secure_asset('img/static/aeps.jpg') }}");
            background-size: cover;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .section-headline p:last-child::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
            margin-bottom: 50px;
        }
        .section-headline p:last-child {
            padding-bottom: 40px;
        }
        .section-headline h2::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0px;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
        }
        .faq-details {
            padding: 30px 0 10px 0;
        }
    </style>
@endsection

@section('content')

    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ secure_asset('img/static/aeps.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- region service area -->
    <div class="faq-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Adhar Enabled Payment System</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div>
                        <div class="" id="p-view-1">
                            <div class="tab-inner">
                                <div class="event-content head-team single-well">
                                    <h4>Background</h4>
                                    <p>
                                        In order to further speed track Financial Inclusion in the country, Two Working Groups were constituted by RBI on MicroATM standards and Central Infrastructure & Connectivity for Aadhaar based financial inclusion transactions with members representing RBI, Unique Identification Authority of India, NPCI, Institute for Development and Research in Banking Technology and some special invitees representing banks and research institutions.

                                    </p>
                                    <p>
                                        The working group on MicroATM standards & Central Infrastructure & Connectivity has submitted its report to RBI. As a part of the working group it was proposed to conduct a Lab level Proof of concept (PoC), integrating the authentication & encryption standards of UIDAI, to test the efficacy of MicroATM standards and transactions using Aadhaar before they are put to actual use. The PoC was successfully demonstrated at various venues.

                                    </p>
                                    <p>
                                        AePS is a bank led model which allows online interoperable financial inclusion transaction at PoS (MicroATM) through the Business correspondent of any bank using the Aadhaar authentication.AePS allows you to do six types of transactions.
                                        The only inputs required for a customer to do a transaction under this scenario are:-

                                    </p>

                                    <ul>
                                        <li>
                                            <i class="fa fa-check"></i> IIN (Identifying the Bank to which the customer is associated)
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Aadhaar Number
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Fingerprint captured during their enrollment
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="" id="p-view-1">
                            <div class="tab-inner">
                                <div class="event-content head-team single-well">
                                    <h4>Objectives</h4>
                                    <ul>
                                        <li>
                                            <i class="fa fa-check"></i> To empower a bank customer to use Aadhaar as his/her identity to access his/ her respective Aadhaar enabled bank account and perform basic banking transactions like cash deposit, cash withdrawal, Intrabank or interbank fund transfer, balance enquiry and obtain a mini statement through a Business Correspondent
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> To sub-serve the goal of Government of India (GoI) and Reserve Bank of India (RBI) in furthering Financial Inclusion.
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> To sub-serve the goal of RBI in electronification of retail payments.
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> To enable banks to route the Aadhaar initiated interbank transactions through a central switching and clearing agency.
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> To facilitate disbursements of Government entitlements like NREGA, Social Security pension, Handicapped Old Age Pension etc. of any Central or State Government bodies, using Aadhaar and authentication thereof as supported by UIDAI.
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> To facilitate inter-operability across banks in a safe and secured manner.
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> To build the foundation for a full range of Aadhaar enabled Banking services.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="" id="p-view-1">
                            <div class="tab-inner">
                                <div class="event-content head-team single-well">
                                    <h4>Services Offered by AePS</h4>
                                    <ul>
                                        <li>
                                            <i class="fa fa-check"></i> Cash Withdrawal
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Cash Deposit
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Balance Enquiry
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Aadhaar to Aadhaar Fund Transfer
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Mini Statement
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i> Best Finger Detection
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end Row -->
        </div>
    </div>
    <!-- endregion service area -->
@endsection

@section('scripts-footer')
    <!-- Sticky menu -->
    <script>

    </script>
@endsection
