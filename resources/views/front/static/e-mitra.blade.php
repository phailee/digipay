@extends('front.layouts.front-layout')
@section('style')
    <style>
        .wellcome-area {
            background: rgba(248, 248, 248, 0.8) url("{{ secure_asset('img/static/emitra.jpg') }}");
            background-size: cover;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .section-headline p:last-child::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
            margin-bottom: 50px;
        }
        .section-headline p:last-child {
            padding-bottom: 40px;
        }
        .section-headline h2::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0px;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
        }
    </style>
@endsection

@section('content')

    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ secure_asset('img/static/emitra.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- start pricing area -->
    <div id="pricing" class="pricing-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Scope of the Project </h2>
                    </div>
                    <div class="well-middle">
                        <div class="single-well">
                            <p>
                                DPP Pvt. Ltd. is selected as a Local Service Provider (LSP) to Setting up & managing the kiosks in the urban and rural areas of the Rajasthan state. In the rural areas, at-least one e-Mitra kiosks on each Gram Panchayat and at each Panchayat Samiti level is to be set up and 3-5 kiosks at Jan-Sunvai Kendra at District HQ along with centres at other urban locations in the area. DPP has to provide the training to the kiosk operator. Delivery of all Government assigned services also deliver following services approved by GoR like:
                            </p>
                            <ul>
                                <li>
                                    <i class="fa fa-check"></i> Bhamashah Services
                                    <ol>
                                        <li><i class="fa fa-check"></i> Data updation & enrollment</li>
                                        <li><i class="fa fa-check"></i> Card printing & lamination</li>
                                        <li><i class="fa fa-check"></i> Banking services</li>
                                    </ol>

                                </li>
                                <li>
                                    <i class="fa fa-check"></i> Aadhar enrollment
                                </li>
                                <li>
                                    <i class="fa fa-check"></i> Business Correspondent (BC) services
                                </li>
                                <li>
                                    <i class="fa fa-check"></i> Various digitization/scanning work
                                </li>
                            </ul>
                        </div>
                        <div class="single-blog text-center">
                            <span>
								<a href="javascript:;" class="ready-btn">आवेदन करें (APPLY)</a>
							</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End table area -->
@endsection

@section('scripts-footer')
    <!-- Sticky menu -->
    <script>

    </script>
@endsection
