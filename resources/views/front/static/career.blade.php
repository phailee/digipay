@extends('front.layouts.front-layout')
@section('style')
    <style>
        .wellcome-area {
            background: rgba(248, 248, 248, 0.8) url("{{ asset('img/static/career.jpg') }}");
            background-size: cover;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .section-headline p:last-child::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
            margin-bottom: 50px;
        }
        .section-headline p:last-child {
            padding-bottom: 40px;
        }
        .section-headline h2::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0px;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
        }
    </style>
@endsection

@section('content')

    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ asset('img/static/career.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- start pricing area -->
    <div id="pricing" class="pricing-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Career with us</h2>
                        <p><strong>Application invited for the Position of “District Manager” (Sales & Service) for Pan India Location.</strong></p>
                        <p>
                            <strong>Qualification </strong>: Minimum Graduate in any Stream
                        </p>
                        <p>
                            <strong>Salary </strong>: Performance Based + Incentive
                        </p>

                        <h3><strong>Preferably Male</strong></h3>
                        <p>
                            <strong>Contact email </strong>: Interested People can send their resume on hr@digipaypoint.com
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End table area -->
@endsection

@section('scripts-footer')
    <!-- Sticky menu -->
    <script>

    </script>
@endsection
