@extends('front.layouts.front-layout')
@section('style')
    <style>
        .wellcome-area {
            background: rgba(248, 248, 248, 0.8) url("{{ asset('img/static/contact-us.jpg') }}");
            background-size: cover;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .section-headline p:last-child::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
            margin-bottom: 50px;
        }
        .section-headline p:last-child {
            padding-bottom: 40px;
        }
        .section-headline h2::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0px;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
        }
    </style>
    <link href="{{ asset('plugins/froiden-helper/helper.css') }}" rel="stylesheet"/>
@endsection

@section('content')

    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ secure_asset('img/static/contact-us.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Start contact Area -->
    <div id="contact" class="contact-area">
        <div class="contact-inner area-padding">
            <div class="contact-overly"></div>
            <div class="container ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="section-headline text-center">
                            <h2>Contact us</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- Start contact icon column -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="contact-icon text-center">
                            <div class="single-icon">
                                <i class="fa fa-mobile"></i>
                                <p>
                                    Call: +91 9588023707<br>
                                    Call: +91 9783152475<br>
                                    <span>Monday-Friday (9am-5pm)</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Start contact icon column -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="contact-icon text-center">
                            <div class="single-icon">
                                <i class="fa fa-envelope-o"></i>
                                <p>
                                    Email: sales@digipaypoint.com<br>
                                    <span>Web: www.digipaypoint.com</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Start contact icon column -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="contact-icon text-center">
                            <div class="single-icon">
                                <i class="fa fa-map-marker"></i>
                                <p>
                                    Location: DPP Pvt. Ltd., S-35, 2nd Floor,<br> Shopping Center – 55, Rajat Path, <br>
                                    <span>Mansarover Jaipur- 302020 Rajasthan</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <!-- Start Google Map -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <!-- Start Map -->
                        <div id="google-map" data-latitude="40.713732" data-longitude="-74.0092704"></div>
                        <!-- End Map -->
                    </div>
                    <!-- End Google Map -->

                    <!-- Start  contact -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form contact-form">
                            <div id="sendmessage">Your message has been sent. Thank you!</div>
                            <div id="errormessage"></div>
                            <form action="" method="post" role="form" class="contactForm" id="conatctUs">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    {{--<div class="validation"></div>--}}
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                    {{--<div class="validation"></div>--}}
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                    {{--<div class="validation"></div>--}}
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                                    {{--<div class="validation"></div>--}}
                                </div>
                                <div class="text-center"><button type="submit" onclick="contactUs();return false;">Send Message</button></div>
                            </form>
                        </div>
                    </div>
                    <!-- End Left contact -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Contact Area -->
@endsection

@section('scripts-footer')
    <script src="{{ asset('plugins/froiden-helper/helper.js')}}"></script>
    <!-- Sticky menu -->
    <script>
        function contactUs(){
            $.easyAjax({
                url: "{!! route('front.contact-us-submit') !!}",
                type: "POST",
                data: $("#conatctUs").serialize(),
                container: "#conatctUs",
                // messagePosition: "inline",
                success: function (response) {
                    if(response.status == 'success') {
                        $('#conatctUs')[0].reset();
                        // $('#contact-form').remove();
                    }
                }
            });
        }
    </script>
@endsection
