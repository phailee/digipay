@extends('front.layouts.front-layout')
@section('style')
    <style>
        .wellcome-area {
            background: rgba(248, 248, 248, 0.8) url("{{ secure_asset('img/static/nsdl.jpg') }}");
            background-size: cover;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .line-out p:last-child::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
            margin-bottom: 50px;
        }
        .line-out p:last-child {
            padding-bottom: 40px;
        }
        .section-headline h2::after {
            border: 1px solid #333;
            bottom: -20px;
            content: "";
            left: 0px;
            margin: 0 auto;
            position: absolute;
            right: 0;
            width: 40%;
        }
        .faq-details {
            padding: 30px 0 10px 0;
        }
    </style>
@endsection

@section('content')

    <div class="wellcome-area">
        <div class="well-bg">
            <div class="test-overly"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="wellcome-text">
                            <div class="well-text text-center">
                                <img src="{{ secure_asset('img/static/nsdl.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- region service area -->
    <div class="faq-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 line-out">
                    <div class="section-headline text-center">
                        <h2>PAN card UTIISL & NSDL</h2>

                    </div>
                    <p>
                        Permanent Account number(PAN) is a must have asset for the Indian Citizens. In fact, The foreigner Citizen can also get a PAN card in India. Due to increasing number of Frauds in Tax, the PAN was introduced. Being a loyal and good citizen, You must get your PAN card. Having a PAN card helps you to be tension free from all tax related issues. It also serves as a Identity proof and in some cases, the existence of PAN becomes mandatory. We at Digipay Point are dedicated to help people in applying for New PAN cards, Applying for Duplicate PAN, Making corrections in PAN information and so on.
                    </p>
                </div>
            </div>
            <!-- end Row -->
        </div>
    </div>
    <!-- endregion service area -->
@endsection

@section('scripts-footer')
    <!-- Sticky menu -->
    <script>

    </script>
@endsection
